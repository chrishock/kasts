/**
 * SPDX-FileCopyrightText: 2021 Bart De Vries <bart@mogwai.be>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import QtMultimedia 5.15

import org.kde.kirigami 2.12 as Kirigami

import org.kde.kasts 1.0

Item {
    property int miniplayerheight: Kirigami.Units.gridUnit * 3
    property int progressbarheight: Kirigami.Units.gridUnit / 6
    property int buttonsize: Kirigami.Units.gridUnit * 2
    height: miniplayerheight + progressbarheight

    visible: AudioManager.entry

    // Set background
    Rectangle {
        anchors.fill: parent
        color: Kirigami.Theme.backgroundColor
    }

    // progress bar for limited width (phones)
    Rectangle {
        id: miniprogressbar
        z: 1
        anchors.top: parent.top
        anchors.left: parent.left
        height: parent.progressbarheight
        color: Kirigami.Theme.highlightColor
        width: parent.width * AudioManager.position / AudioManager.duration
        visible: true
    }

    RowLayout {
        id: footerrowlayout
        anchors.topMargin: miniprogressbar.height
        anchors.fill: parent
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                ImageWithFallback {
                    imageSource: AudioManager.entry.cachedImage
                    Layout.preferredHeight: miniplayerheight
                    Layout.preferredWidth: miniplayerheight
                }

                // track information
                ColumnLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.leftMargin: Kirigami.Units.smallSpacing
                    Controls.Label {
                        id: mainLabel
                        text: AudioManager.entry.title
                        wrapMode: Text.Wrap
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignLeft
                        elide: Text.ElideRight
                        maximumLineCount: 1
                        //font.weight: Font.Bold
                        font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1
                    }

                    Controls.Label {
                        id: feedLabel
                        text: AudioManager.entry.feed.name
                        wrapMode: Text.Wrap
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignLeft
                        elide: Text.ElideRight
                        maximumLineCount: 1
                        opacity: 0.6
                        font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1
                    }
                }
            }
            MouseArea {
                id: trackClick
                anchors.fill: parent
                hoverEnabled: true
                onClicked: toOpen.restart()
            }
        }
        Controls.Button {
            id: playButton
            icon.name: AudioManager.playbackState === Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
            icon.height: parent.parent.buttonsize
            icon.width: parent.parent.buttonsize
            flat: true
            Layout.fillHeight: true
            Layout.maximumHeight: parent.parent.miniplayerheight
            Layout.maximumWidth: height
            onClicked: AudioManager.playbackState === Audio.PlayingState ? AudioManager.pause() : AudioManager.play()
            Layout.alignment: Qt.AlignVCenter
        }
    }
}

